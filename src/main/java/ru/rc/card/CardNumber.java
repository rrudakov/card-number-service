package ru.rc.card;

public class CardNumber {
  public CardNumber() {
  }

  public CardNumber(Integer id, String cardNumber, String cifId) {
    this.id = id;
    this.cardNumber = cardNumber;
    this.cifId = cifId;
  }

  private Integer id;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  private String cardNumber;

  public String getCardNumber() {
    return this.cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }


  private String cifId;

  public String getCifId() {
    return this.cifId;
  }

  public void setCifId(String cifId) {
    this.cifId = cifId;
  }
}
