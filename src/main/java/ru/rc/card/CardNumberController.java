package ru.rc.card;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardNumberController {

  @RequestMapping("/getCardNumber")
  public CardNumber getCardNumber() {
    Connection conn = null;
    Statement st = null;
    CardNumber result = new CardNumber();

    try {
      Class.forName("org.sqlite.JDBC");
      conn = DriverManager.getConnection("jdbc:sqlite:cards.db");
      conn.setAutoCommit(false);

      st = conn.createStatement();
      ResultSet rs = st.executeQuery("SELECT * FROM card_numbers ORDER BY id LIMIT 1");

      while (rs.next()) {
        result.setId(rs.getInt("id"));
        result.setCardNumber(rs.getString("card_number"));
        result.setCifId(rs.getString("cif_id"));
      }

      rs.close();

      if (result.getId() != null) {
        st.executeUpdate(String.format("DELETE FROM card_numbers WHERE id=%d", result.getId()));
        conn.commit();
      }
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        st.close();
        conn.close();
      } catch (SQLException e) {}
    }

    return result;
  }
}
